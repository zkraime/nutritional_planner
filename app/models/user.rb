class User < ActiveRecord::Base
    has_many :microposts, dependent: :destroy
    VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
    validates :name,  presence: true, length: { maximum: 55 }
    validates :phone_number,  presence: true
    validates :email, presence: true, length: { maximum: 250 },
        format: { with: VALID_EMAIL_REGEX },
                 uniqueness: { case_sensitive: false }
    has_secure_password
    validates :password, length: { minimum: 6 }, allow_blank: true
    
    
    def feed
        Micropost.where("user_id = ?", id)
    end
end
