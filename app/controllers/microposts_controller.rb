class MicropostsController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy]
  
  def create
    @micropost = current_user.microposts.build(micropost_params)
    if @micropost.save
      flash[:success] = "Note created!"
      redirect_to root_url
    else
      @feed_items = []
      render 'static_pages/home'
    end
  end
  
  def destroy
    @status_update = Micropost.find(params[:id]) #ActiveRecord::Base.connection.execute(SELECT * FROM micropost WHERE (micropost.id = params[:id]) LIMIT 1)
    if @status_update.present?
      @status_update.destroy
    end
 #   redirect_to root_url
   # @micropost.destroy
    flash[:success] = "Note deleted!"
    redirect_to request.referrer || root_url
  end
#this comment is unimportant

  private
  
    def micropost_params
      params.require(:micropost).permit(:content, :picture)
    end
  
end
